package com.senhorpastel.message.sender.service;

import com.senhorpastel.message.sender.dto.RequestDto;
import com.senhorpastel.message.sender.exception.StorageException;
import com.senhorpastel.message.sender.model.MessageProcess;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Service
public class BrowserService {

    private static final Logger log = LoggerFactory.getLogger(BrowserService.class);
    private static final String urlMessage = "https://api.whatsapp.com/send?phone=";

    @Autowired
    private StorageService storageService;


    public int prepareMessage(MessageProcess requestDto) {
        WebDriver driver = getChromeInstance();
        int count = 0;
        BufferedReader br = null;
        String line = "";
        String splitBy = ";";
        try {
            br = new BufferedReader(new FileReader(new File(requestDto.getFilePath())));
            while ((line = br.readLine()) != null) {
                String[] msgParams = line.split(splitBy);
                String finalMsg = requestDto.getMessageText();
                for (int i = 0; i < msgParams.length - 1; i++) {
                   finalMsg = finalMsg.replaceFirst("#\\d+", msgParams[i + 1]);
                }
                Thread.sleep(5000);
                count = count + sendWhatsappMessage(msgParams[0], finalMsg, driver);
            }
            driver.close();
            return count;
        } catch (IOException | InterruptedException e) {
            log.error("Error sending message", e);
            driver.close();
            return count;
        }
    }

    private int sendWhatsappMessage(String msisdn, String msg, WebDriver driver)  {
        try {
            String url = urlMessage + msisdn;
            driver.get(url);
            WebElement newConversation = (new WebDriverWait(driver, 20))
                    .until(ExpectedConditions.presenceOfElementLocated(By.id("action-button")));
            newConversation.click();

            WebElement webWhatsapp = (new WebDriverWait(driver, 20))
                    .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"fallback_block\"]/div/div/a")));
            webWhatsapp.click();

            //wait authentication for 20 seconds before look for text field
            Thread.sleep(2000);
            WebElement textField = (new WebDriverWait(driver, 36000))
                    .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]")));
            textField.click();
            textField.sendKeys(msg);

            //attach pictures
           /* WebElement attachIcon = (new WebDriverWait(driver, 20))
                    .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"main\"]/header/div[3]/div/div[2]/div")));
            attachIcon.click();

            WebElement attachDocument = (new WebDriverWait(driver, 20))
                    .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"app\"]/div/div/div[2]/div[2]/span/div/span/div/div/div[2]/input")));
            attachDocument.sendKeys(picturesPath);
            Thread.sleep(2000);*/

            WebElement sendIcon = driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[3]/button/span"));
            sendIcon.click();
            return 1;
        } catch (Exception ex){
            log.error(String.format("Erro ao tentar enviar mensagem para o número %s", msisdn), ex);
            return 0;
        }
    }


    private WebDriver getChromeInstance() {
        log.debug("Creating chrome driver instance...");
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return driver;
    }
}
