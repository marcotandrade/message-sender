package com.senhorpastel.message.sender.service;

import com.senhorpastel.message.sender.exception.StorageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Calendar;
import java.util.GregorianCalendar;

@Service
public class StorageService {

    private static final Logger log = LoggerFactory.getLogger(StorageService.class);

    @Value("${upload.path}")
    private String path;

    public String uploadFile(MultipartFile file) throws StorageException {

        if (file.isEmpty()) {
            throw new StorageException("Failed to store empty file");
        }
        try {
            String fileName = file.getOriginalFilename();
            InputStream is = file.getInputStream();
            Calendar fileDate = new GregorianCalendar();
            long timeValue = fileDate.getTimeInMillis();
            String finalName = path + fileName.replace(".csv", "_" + timeValue + ".csv");
            Files.copy(is, Paths.get(finalName),
                    StandardCopyOption.REPLACE_EXISTING);
            is.close();
            return finalName;
        } catch (IOException e) {
            String msg = String.format("Failed to store file %s", file.getName());
            log.error(msg, e);
            throw new StorageException(msg, e);
        }
    }


}
