package com.senhorpastel.message.sender.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

public class RequestDto implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(RequestDto.class);
    private static final long serialVersionUID = -1640384604415858849L;

    private String message;
    private String filePath;
    private MultipartFile csvFile;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MultipartFile getCsvFile() {
        return csvFile;
    }

    public void setCsvFile(MultipartFile csvFile) {
        this.csvFile = csvFile;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public RequestDto() {
    }

    public RequestDto(String message, MultipartFile csvFile, String path) {
        this.message = message;
        this.csvFile = csvFile;
        this.filePath = path;
    }

    @Override
    public String toString() {
        return "RequestDto{" +
                "message='" + message + '\'' +
                ", filePath='" + filePath + '\'' +
                ", csvFile=" + csvFile +
                '}';
    }
}
