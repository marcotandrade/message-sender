package com.senhorpastel.message.sender.rabbitmq;

import com.senhorpastel.message.sender.model.MessageProcess;
import com.senhorpastel.message.sender.repository.MessageProcessRepository;
import com.senhorpastel.message.sender.service.BrowserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.concurrent.CountDownLatch;

@Component
public class Receiver {

    private static final Logger log = LoggerFactory.getLogger(Receiver.class);
    private CountDownLatch latch = new CountDownLatch(1);

    @Autowired
    private MessageProcessRepository messageProcessRepository;

    @Autowired
    private BrowserService browserService;

    public void receiveMessage(Long message) {
        log.info("Received from queue <" + message + ">");
        process(message);
        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    private void process(Long messageId) {
        try {
            String response = null;
            long startTime = System.currentTimeMillis();
            log.info("Initialize message sending process...");
            Optional<MessageProcess> msp = messageProcessRepository.findById(messageId);
            if (msp.isPresent()) {
                int messageQtt = browserService.prepareMessage(msp.get());

                long endTime = System.currentTimeMillis();
                response = String.format("%s messages have been sent in  %s ms!", messageQtt, (endTime - startTime));
                log.info(response);
            } else {
                log.warn(String.format("No message found on memory database with id: %s!", messageId));
            }
        } catch (Exception ex) {
            log.error("Erro genérico ao processar mensagem!", ex);
            latch.countDown();
        }
    }
}
