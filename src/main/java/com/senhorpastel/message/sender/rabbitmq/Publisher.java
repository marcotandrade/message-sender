package com.senhorpastel.message.sender.rabbitmq;

import com.senhorpastel.message.sender.MessageSenderApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class Publisher {
    private static final Logger log = LoggerFactory.getLogger(Publisher.class);

    private final RabbitTemplate rabbitTemplate;
    private final Receiver receiver;

    public Publisher(Receiver receiver, RabbitTemplate rabbitTemplate) {
        this.receiver = receiver;
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendToQueue(Long obj) throws InterruptedException {
        log.debug("Sending Message to queue..<" + obj.toString() + ">");
        rabbitTemplate.convertAndSend(MessageSenderApplication.topicExchangeName, "foo.bar.baz", obj);
        receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);
    }
}
