package com.senhorpastel.message.sender.model;

import com.senhorpastel.message.sender.dto.RequestDto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.IOException;
import java.util.Arrays;

@Entity
@Table(name = "MESSAGE_PROCESS")
public class MessageProcess {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", length = 20)
    private Long id;

    @Column(name = "MESSAGE", nullable = false, length = 4000)
    private String messageText;

    @Column(name = "FILE_PATH", nullable = false, length = 500)
    private String filePath;

    @Column(name = "FILE", nullable = false)
    private byte[] csvFile;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public byte[] getCsvFile() {
        return csvFile;
    }

    public void setCsvFile(byte[] csvFile) {
        this.csvFile = csvFile;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public MessageProcess(RequestDto requestDto) throws IOException {
        this.messageText = requestDto.getMessage();
        this.csvFile = requestDto.getCsvFile().getBytes();
        this.filePath = requestDto.getFilePath();
    }

    public MessageProcess() {
    }

    @Override
    public String toString() {
        return "MessageProcess{" +
                "id=" + id +
                ", messageText='" + messageText + '\'' +
                ", csvFile=" + Arrays.toString(csvFile) +
                '}';
    }
}
