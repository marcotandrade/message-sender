package com.senhorpastel.message.sender.repository;

import com.senhorpastel.message.sender.model.MessageProcess;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MessageProcessRepository extends PagingAndSortingRepository<MessageProcess, Long> {

}
