package com.senhorpastel.message.sender.controller;

import com.senhorpastel.message.sender.dto.RequestDto;
import com.senhorpastel.message.sender.model.MessageProcess;
import com.senhorpastel.message.sender.rabbitmq.Publisher;
import com.senhorpastel.message.sender.repository.MessageProcessRepository;
import com.senhorpastel.message.sender.service.BrowserService;
import com.senhorpastel.message.sender.service.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@RestController
@RequestMapping("ws")
public class MessageController {

    private static final Logger log = LoggerFactory.getLogger(MessageController.class);

    @Autowired
    private BrowserService browserService;

    @Autowired
    private Publisher publisher;

    @Autowired
    StorageService storageService;

    @Value("${upload.path}")
    private String pathDir;


    @Autowired
    private MessageProcessRepository messageProcessRepository;

    @PostMapping(value = "/send", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            MediaType.MULTIPART_FORM_DATA_VALUE})
    public ModelAndView sendMessage(@Valid RequestDto requestDto, BindingResult result, Model model) {
        String response = "error";
        if (result.hasErrors()) {
            //todo
            return new ModelAndView("menu-message", "requestDto", new RequestDto());
        }
        try {
            String savedFile = storageService.uploadFile(requestDto.getCsvFile());
            requestDto.setFilePath(savedFile);
            MessageProcess messageEntity = new MessageProcess(requestDto);
            MessageProcess persisted = messageProcessRepository.save(messageEntity);
            publisher.sendToQueue(persisted.getId());
            return new ModelAndView("menu-message", "requestDto", new RequestDto());
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return null;
    }
}
