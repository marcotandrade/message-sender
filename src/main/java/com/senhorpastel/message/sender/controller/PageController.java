package com.senhorpastel.message.sender.controller;

import com.senhorpastel.message.sender.dto.RequestDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PageController {

    private static final Logger log = LoggerFactory.getLogger(PageController.class);

    @RequestMapping("/menu-message")
    public ModelAndView getMessagePage() {
        return new ModelAndView("menu-message", "requestDto", new RequestDto());
    }






}
