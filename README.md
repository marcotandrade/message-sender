# message-sender
Automação de envio de mensagens através do whatsapp web, para que isso possa ser feito de forma gratuita.

### Pré requisitos do sistemas
 - instalação da linguagem erlang
 - instalação rabbitmq server
 - criar pasta "message-sender" no C:/
 - Instalação do Google Chrome (versões homologadas 71,72,73)
 
#### Frameworks
 - Spring Boot
 - Spring Data JPA
 - Spring MVC
 - RabbitMQ
 - H2 Database
 - Selenium
 - Thymeleaf
 - Bootstrap

 
 
 